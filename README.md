# tracker

![](https://img.shields.io/badge/written%20in-C-blue)

An audio sequencer.

This is a port of Per-Olov Jernberg's JS1K entry "1024 byte sequencer" ( http://js1k.com/2013-spring/details/1506 ) including the same song. The source code is somewhat longer.

It generates 44.1kHz audio and plays via winmm.

The song can be edited in `song.inc` using the same `N` and `C` helper functions as described in the JS1K entry.

Tested with i686-w64-mingw32-gcc under cygwin64.

## Changelog

2016-12-18 0.1
- Initial release
- [⬇️ tracker-0.1.zip](dist-archive/tracker-0.1.zip) *(11.14 KiB)*

